import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router';
import Vuetify from 'vuetify';
// import { router } from './config';
// import 'vuetify/dist/vuetify.min.css'
// import 'material-design-icons-iconfont/dist/material-design-icons.css'

import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.css'

import VueGoogleCharts from 'vue-google-charts'
Vue.use(VueGoogleCharts)

Vue.use(VueMaterial)

Vue.use(Vuetify)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router: router,
}).$mount('#app')
